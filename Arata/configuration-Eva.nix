{ config, pkgs, ... }:

let
  unstable = import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz) {};
in

{
  imports = [
    ./hardware-configuration.nix
  ];

  # HARDWARE
  hardware = {
    cpu.intel.updateMicrocode = true;

    opengl.enable = true;
  };

  time.timeZone = "Europe/Rome";

  # BOOT
  boot = {
    cleanTmpDir = true;

    loader.grub.device = "/dev/sda";
  };

  # SYSTEM

  # gui
  services.xserver = {
    enable = true;

    layout = "it";
    xkbOptions = "eurosign:e";
    i18n = {
      consoleFont = "Lat2-Terminus16";
      consoleKeyMap = "it";
      defaultLocale = "it_IT.UTF-8";
    };

    displayManager.gdm.enable = true;

    desktopManager.gnome3.enable = true;
    desktopManager.plasma5.enable = true;
  };

  environment.systemPackages = with pkgs; [
    xwayland
  ];

  # network
  networking = {
    hostName = "ArataPC";

    networkmanager = {
      enable = true;
    };
  };

  services.resolved.enable = true;

  # update
  nix = {
    gc = {
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 30d";
    };

    optimise = {
      automatic = true;
      dates = [ "daily" ];
    };

    maxJobs = 2;
  };

  systemd.services.nixos-upgrade = {
    description = "NixOS Upgrade";

    restartIfChanged = false;
    unitConfig.X-StopOnRemoval = false;

    serviceConfig.Type = "oneshot";

    environment = config.nix.envVars //
                  {
                    inherit (config.environment.sessionVariables) NIX_PATH;
                    HOME = "/root";
                  } // config.networking.proxy.envVars;

    path = with pkgs; [ coreutils gnutar xz.bin gzip gitMinimal config.nix.package.out ];

    script = let
      nixos-rebuild = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild";
    in
      '' ${nixos-rebuild} boot --upgrade '';

    wants = ["network-online.target"];
    after = ["network-online.target"];

    startAt = "daily";
  };

  appstream.enable = true;

  environment.systemPackages = with pkgs; [

    nixops

    ed
    file
    htop
    inxi
    killall
    lshw
    lolcat
    mkpasswd
    neofetch
    parted
    pciutils
    tmux
    udisks
    usbutils
    wget

    gparted
  ];

  # USERS
  users.users.adamo = { pkgs, ... }: {
    extraGroups = [ "audio" "networkmanager" "wheel" ];
    passwordFile = "users-adamo";

    isNormalUser = true;
  };

  home-manager.users.adamo = { pkgs, ...}: {

    programs.home-manager.enable = true;
    nixpkgs.config.allowUnfree = true;

    home.packages = with pkgs; [

      libreoffice-fresh

      spotify
      discord
      telegram
      sky

      firefox
      chromium

      neovim
      emacs
      vscodium
      gnome-builder
      geany

      git
      smartgit
      git-cola

      gcc
      make
      cmake
      ninja
      meson
      eclipses.eclipse-cpp

      go
      gocode

      ghc

      openjdk
      gradle
      gradle-completion
      eclipses.eclipse-java

      texlive.combined.scheme-small
      texstudio
      gnome-latex

      ocaml
      ocamlPackages.merlin

      python
      pypy3

      logisim
    ];

    programs.neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
    };
  };
}
