# MBR/BIOS
parted /dev/sda -- mklabel msdos

# PARTITIONING
parted /dev/sda -- mkpart primary 1MiB -8GiB
parted /dev/sda -- mkpart primary linux-swap -8GiB 100%

mkfs.ext4 -L nixos /dev/sda1
mkswap -L swap /dev/sda2

# INSTALLING
mount /dev/disk/by-label/nixos /mnt
swapon /dev/sda2
nixos-generate-config --root /mnt

nix-env -iA nixos.git
git clone https://gitlab.com/DunkleCat/nixos-configurations.git
cd nixos-configurations
cp Arata/configuration-Human.nix /mnt/etc/nixos/configuration.nix
cp users-utente /mnt/etc/nixos/users-utente

nixos-install
